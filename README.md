# Flectra Community / intrastat-extrastat

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[intrastat_base](intrastat_base/) | 2.0.2.2.0| Base module for Intrastat reporting
[product_harmonized_system_delivery](product_harmonized_system_delivery/) | 2.0.1.2.0| Hide native hs_code field provided by the delivery module
[intrastat_product_generic](intrastat_product_generic/) | 2.0.1.0.0| Generic Intrastat Product Declaration
[product_harmonized_system_stock](product_harmonized_system_stock/) | 2.0.1.0.0| Adds a menu entry for H.S. codes
[product_harmonized_system](product_harmonized_system/) | 2.0.2.3.0| Base module for Product Import/Export reports
[intrastat_product](intrastat_product/) | 2.0.1.6.1| Base module for Intrastat Product


